package ru.handh.homeservice.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import ru.handh.homeservice.dto.request.HomeRequest;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.exception.HomeNotFoundException;
import ru.handh.homeservice.repository.HomeRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class HomeServiceImplTest {

    @InjectMocks
    private HomeServiceImpl homeService;
    @Mock
    private HomeRepository homeRepository;
    @Mock
    private ConversionService conversionService;

    @Test
    void create() {
        int ownerId = 1;
        HomeRequest homeRequest = new HomeRequest();
        homeRequest.setName("name");
        homeRequest.setAddress("address");
        HomeEntity expected = new HomeEntity();
        expected.setName("name");
        expected.setAddress("address");
        Mockito.when(conversionService.convert(homeRequest, HomeEntity.class)).thenReturn(expected);
        expected.setOwnerId(ownerId);
        Mockito.when(homeRepository.save(expected)).thenReturn(expected);

        HomeEntity actual = homeService.create(homeRequest, ownerId);

        assertEquals(expected, actual);
    }

    @Test
    void edit() {
        int homeId = 1;
        HomeRequest homeRequest = new HomeRequest();
        homeRequest.setName("name");
        homeRequest.setAddress("address");
        HomeEntity found = new HomeEntity();
        found.setId(homeId);
        found.setName("found");
        found.setAddress("found");
        found.setOwnerId(6);
        Mockito.when(homeRepository.findById(homeId)).thenReturn(Optional.of(found));
        HomeEntity expected = new HomeEntity();
        expected.setId(homeId);
        expected.setName("name");
        expected.setAddress("address");
        expected.setOwnerId(6);
        Mockito.when(homeRepository.save(expected)).thenReturn(expected);

        HomeEntity actual = homeService.edit(homeId, homeRequest);

        assertEquals(expected, actual);
    }

    @Test
    void edit_shouldThrowHomeNotFoundException_whenHomeNotExists() {
        int homeId = 1;
        HomeRequest homeRequest = new HomeRequest();
        homeRequest.setName("name");
        homeRequest.setAddress("address");
        Mockito.when(homeRepository.findById(homeId)).thenReturn(Optional.empty());

        assertThrows(HomeNotFoundException.class, () -> homeService.edit(homeId, homeRequest));
    }

    @Test
    void findById() {
        int homeId = 1;
        HomeEntity expected = new HomeEntity();
        Mockito.when(homeRepository.findById(homeId)).thenReturn(Optional.of(expected));

        HomeEntity actual = homeService.findById(homeId);

        assertEquals(expected, actual);
    }

    @Test
    void findById_shouldThrowHomeNotFoundException_whenHomeNotExists() {
        int homeId = 1;
        Mockito.when(homeRepository.findById(homeId)).thenReturn(Optional.empty());

        assertThrows(HomeNotFoundException.class, () -> homeService.findById(homeId));
    }

    @Test
    void findAll() {
        HomeEntity first = new HomeEntity();
        HomeEntity second = new HomeEntity();
        List<HomeEntity> expected = List.of(first, second);
        Mockito.when(homeRepository.findAll()).thenReturn(expected);

        List<HomeEntity> actual = homeService.findAll();

        assertEquals(expected, actual);
    }

    @Test
    void deleteById() {
        int homeId = 1;
        HomeEntity home = new HomeEntity();
        home.setId(homeId);
        Mockito.when(homeRepository.findById(homeId)).thenReturn(Optional.of(home));

        homeService.deleteById(homeId);

        Mockito.verify(homeRepository).delete(home);
    }

    @Test
    void deleteById_shouldThrowHomeNotFoundException_whenHomeNotExists() {
        int homeId = 1;
        Mockito.when(homeRepository.findById(homeId)).thenReturn(Optional.empty());

        assertThrows(HomeNotFoundException.class, () -> homeService.deleteById(homeId));
    }

    @Test
    void deleteByOwnerId() {
        int ownerId = 1;

        homeService.deleteByOwnerId(ownerId);

        Mockito.verify(homeRepository).deleteByOwnerId(ownerId);
    }
}