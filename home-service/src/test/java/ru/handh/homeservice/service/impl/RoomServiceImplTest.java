package ru.handh.homeservice.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import ru.handh.homeservice.dto.request.RoomRequest;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.entity.RoomEntity;
import ru.handh.homeservice.exception.HomeNotFoundException;
import ru.handh.homeservice.exception.RoomNotFoundException;
import ru.handh.homeservice.repository.RoomRepository;
import ru.handh.homeservice.service.HomeService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RoomServiceImplTest {

    @InjectMocks
    private RoomServiceImpl roomService;
    @Mock
    private HomeService homeService;
    @Mock
    private RoomRepository roomRepository;
    @Mock
    private ConversionService conversionService;

    @Test
    void create() {
        int homeId = 1;
        RoomRequest roomRequest = new RoomRequest();
        roomRequest.setName("room");
        HomeEntity home = new HomeEntity();
        Mockito.when(homeService.findById(homeId)).thenReturn(home);
        RoomEntity roomConverted = new RoomEntity();
        roomConverted.setName("room");
        Mockito.when(conversionService.convert(roomRequest, RoomEntity.class)).thenReturn(roomConverted);
        RoomEntity expected = new RoomEntity();
        expected.setName("room");
        expected.setHome(home);

        RoomEntity actual = roomService.create(roomRequest, homeId);

        assertEquals(expected, actual);
    }

    @Test
    void create_shouldThrowHomeNotFoundException_whenHomeNotExists() {
        int homeId = 1;
        RoomRequest roomRequest = new RoomRequest();
        roomRequest.setName("room");
        Mockito.when(homeService.findById(homeId)).thenThrow(HomeNotFoundException.class);

        assertThrows(HomeNotFoundException.class, () -> roomService.create(roomRequest, homeId));
    }

    @Test
    void edit() {
        int roomId = 1;
        RoomRequest roomRequest = new RoomRequest();
        roomRequest.setName("new");
        RoomEntity found = new RoomEntity();
        found.setName("old");
        Mockito.when(roomRepository.findById(roomId)).thenReturn(Optional.of(found));
        RoomEntity expected = new RoomEntity();
        expected.setName("new");
        Mockito.when(roomRepository.save(expected)).thenReturn(expected);

        RoomEntity actual = roomService.edit(roomId, roomRequest);

        assertEquals(expected, actual);
    }

    @Test
    void edit_shouldThrowRoomNotFoundException_whenRoomNotExists() {
        int roomId = 1;
        RoomRequest roomRequest = new RoomRequest();
        roomRequest.setName("new");
        Mockito.when(roomRepository.findById(roomId)).thenReturn(Optional.empty());

        assertThrows(RoomNotFoundException.class, () -> roomService.edit(roomId, roomRequest));
    }

    @Test
    void deleteById() {
        int roomId = 1;
        RoomEntity found = new RoomEntity();
        Mockito.when(roomRepository.findById(roomId)).thenReturn(Optional.of(found));

        roomService.deleteById(roomId);

        Mockito.verify(roomRepository).delete(found);
    }

    @Test
    void deleteById_shouldThrowRoomNotFoundException_whenRoomNotExists() {
        int roomId = 1;
        Mockito.when(roomRepository.findById(roomId)).thenReturn(Optional.empty());

        assertThrows(RoomNotFoundException.class, () -> roomService.deleteById(roomId));
    }

    @Test
    void findById() {
        int roomId = 1;
        RoomEntity expected = new RoomEntity();
        Mockito.when(roomRepository.findById(roomId)).thenReturn(Optional.of(expected));

        RoomEntity actual = roomService.findById(roomId);

        assertEquals(expected, actual);
    }

    @Test
    void findById_shouldThrowRoomNotFoundException_whenRoomNotExists() {
        int roomId = 1;
        Mockito.when(roomRepository.findById(roomId)).thenReturn(Optional.empty());

        assertThrows(RoomNotFoundException.class, () -> roomService.findById(roomId));
    }
}