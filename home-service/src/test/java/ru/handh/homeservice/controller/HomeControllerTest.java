package ru.handh.homeservice.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.homeservice.dto.request.HomeRequest;
import ru.handh.homeservice.dto.response.HomeDetailed;
import ru.handh.homeservice.dto.response.HomeSimple;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.exception.HomeNotFoundException;
import ru.handh.homeservice.service.HomeService;
import ru.handh.homeservice.utils.AuthorizationUtil;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class HomeControllerTest {

    @InjectMocks
    private HomeController homeController;
    @Mock
    private HomeService homeService;
    @Mock
    private ConversionService conversionService;
    @Mock
    private AuthorizationUtil authorizationUtil;
    @Mock
    private KafkaTemplate<String, Integer> kafkaTemplate;


    @Test
    void createHome() {
        HomeRequest homeRequest = new HomeRequest("home", "addr");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("home");
        home.setAddress("addr");
        home.setOwnerId(1);
        Mockito.when(homeService.create(homeRequest, 1)).thenReturn(home);
        HomeDetailed expected = new HomeDetailed(10, "home", "addr", Collections.emptyList());
        Mockito.when(conversionService.convert(home, HomeDetailed.class)).thenReturn(expected);

        HomeDetailed actual = homeController.createHome(homeRequest);

        assertEquals(expected, actual);
    }

    @Test
    void editHome() {
        int homeId = 10;
        HomeRequest homeRequest = new HomeRequest("newname", "newaddr");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("old");
        home.setAddress("old");
        home.setOwnerId(1);
        Mockito.when(homeService.findById(homeId)).thenReturn(home);
        HomeEntity homeEdited = new HomeEntity();
        home.setId(10);
        home.setName("newname");
        home.setAddress("newaddr");
        home.setOwnerId(1);
        Mockito.when(homeService.edit(homeId, homeRequest)).thenReturn(homeEdited);
        HomeDetailed expected = new HomeDetailed();
        expected.setId(10);
        expected.setName("newname");
        expected.setAddress("newaddr");
        Mockito.when(conversionService.convert(homeEdited, HomeDetailed.class)).thenReturn(expected);

        HomeDetailed actual = homeController.editHome(homeId, homeRequest);

        assertEquals(expected, actual);
    }

    @Test
    void editHome_shouldThrowException_whenUserIsNotOwner() {
        int homeId = 10;
        HomeRequest homeRequest = new HomeRequest("newname", "newaddr");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("old");
        home.setAddress("old");
        home.setOwnerId(3);
        Mockito.when(homeService.findById(homeId)).thenReturn(home);

        assertThrows(ResponseStatusException.class, () -> homeController.editHome(homeId, homeRequest));
    }

    @Test
    void editHome_shouldThrowException_whenHomeNotFound() {
        int homeId = 10;
        HomeRequest homeRequest = new HomeRequest("newname", "newaddr");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        Mockito.when(homeService.findById(homeId)).thenThrow(HomeNotFoundException.class);

        assertThrows(HomeNotFoundException.class, () -> homeController.editHome(homeId, homeRequest));
    }

    @Test
    void getHome() {
        int homeId = 10;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("name");
        home.setAddress("addr");
        home.setOwnerId(1);
        Mockito.when(homeService.findById(homeId)).thenReturn(home);
        HomeDetailed expected = new HomeDetailed();
        expected.setId(10);
        expected.setName("name");
        expected.setAddress("addr");
        Mockito.when(conversionService.convert(home, HomeDetailed.class)).thenReturn(expected);

        HomeDetailed actual = homeController.getHome(homeId);

        assertEquals(expected, actual);
    }

    @Test
    void getHome_shouldThrowException_whenUserIsNotOwner() {
        int homeId = 10;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(3);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("name");
        home.setAddress("addr");
        home.setOwnerId(1);
        Mockito.when(homeService.findById(homeId)).thenReturn(home);

        assertThrows(ResponseStatusException.class, () -> homeController.getHome(homeId));
    }

    @Test
    void getHome_shouldThrowException_whenHomeNotFound() {
        int homeId = 10;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(3);
        Mockito.when(homeService.findById(homeId)).thenThrow(HomeNotFoundException.class);

        assertThrows(HomeNotFoundException.class, () -> homeController.getHome(homeId));
    }

    @Test
    void getHomesList() {
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(3);
        HomeEntity first = new HomeEntity();
        first.setId(1);
        first.setName("first");
        first.setOwnerId(3);
        HomeEntity second = new HomeEntity();
        second.setId(2);
        second.setName("second");
        second.setOwnerId(2);
        HomeEntity third = new HomeEntity();
        third.setId(3);
        third.setName("third");
        third.setOwnerId(3);
        List<HomeEntity> given = List.of(first, second, third);
        Mockito.when(homeService.findAll()).thenReturn(given);
        HomeSimple firstSimple = new HomeSimple(1, "first");
        Mockito.when(conversionService.convert(first, HomeSimple.class)).thenReturn(firstSimple);
        HomeSimple thirdSimple = new HomeSimple(3, "third");
        Mockito.when(conversionService.convert(third, HomeSimple.class)).thenReturn(thirdSimple);

        List<HomeSimple> actual = homeController.getHomesList();

        assertEquals(2, actual.size());
        assertTrue(actual.contains(firstSimple));
        assertTrue(actual.contains(thirdSimple));
    }

    @Test
    void deleteHome() {
        int homeId = 10;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("name");
        home.setAddress("addr");
        home.setOwnerId(1);
        Mockito.when(homeService.findById(homeId)).thenReturn(home);

        ResponseEntity<HttpStatus> actual = homeController.deleteHome(homeId);

        Mockito.verify(homeService).deleteById(homeId);
        assertTrue(actual.getStatusCode().is2xxSuccessful());
    }

    @Test
    void deleteHome_shouldThrowException_whenUserIsNotOwner() {
        int homeId = 10;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(3);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("name");
        home.setAddress("addr");
        home.setOwnerId(1);
        Mockito.when(homeService.findById(homeId)).thenReturn(home);

        assertThrows(ResponseStatusException.class, () -> homeController.deleteHome(homeId));
    }

    @Test
    void deleteHome_shouldThrowException_whenHomeNotFound() {
        int homeId = 10;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(3);
        Mockito.when(homeService.findById(homeId)).thenThrow(HomeNotFoundException.class);

        assertThrows(HomeNotFoundException.class, () -> homeController.deleteHome(homeId));
    }
}