package ru.handh.homeservice.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.homeservice.dto.request.RoomRequest;
import ru.handh.homeservice.dto.response.RoomDetailed;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.entity.RoomEntity;
import ru.handh.homeservice.exception.HomeNotFoundException;
import ru.handh.homeservice.exception.RoomNotFoundException;
import ru.handh.homeservice.service.HomeService;
import ru.handh.homeservice.service.RoomService;
import ru.handh.homeservice.utils.AuthorizationUtil;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RoomControllerTest {

    @InjectMocks
    private RoomController roomController;
    @Mock
    private RoomService roomService;
    @Mock
    private HomeService homeService;
    @Mock
    private ConversionService conversionService;
    @Mock
    private AuthorizationUtil authorizationUtil;
    @Mock
    private KafkaTemplate<String, Integer> kafkaTemplate;

    @Test
    void createRoom() {
        int homeId = 10;
        RoomRequest roomRequest = new RoomRequest("room");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("home");
        home.setAddress("addr");
        home.setOwnerId(1);
        Mockito.when(homeService.findById(homeId)).thenReturn(home);
        RoomEntity room = new RoomEntity();
        room.setId(5);
        room.setName("room");
        room.setHome(home);
        Mockito.when(roomService.create(roomRequest, homeId)).thenReturn(room);
        RoomDetailed expected = new RoomDetailed(5, "room");
        Mockito.when(conversionService.convert(room, RoomDetailed.class)).thenReturn(expected);

        RoomDetailed actual = roomController.createRoom(homeId, roomRequest);

        assertEquals(expected, actual);
    }

    @Test
    void createRoom_shouldThrowException_whenUserIsNotOwner() {
        int homeId = 10;
        RoomRequest roomRequest = new RoomRequest("room");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("home");
        home.setAddress("addr");
        home.setOwnerId(3);
        Mockito.when(homeService.findById(homeId)).thenReturn(home);

        assertThrows(ResponseStatusException.class, () -> roomController.createRoom(homeId, roomRequest));
    }

    @Test
    void createRoom_shouldThrowException_whenHomeNotFound() {
        int homeId = 10;
        RoomRequest roomRequest = new RoomRequest("room");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        Mockito.when(homeService.findById(homeId)).thenThrow(HomeNotFoundException.class);

        assertThrows(HomeNotFoundException.class, () -> roomController.createRoom(homeId, roomRequest));
    }

    @Test
    void editRoom() {
        int roomId = 5;
        RoomRequest roomRequest = new RoomRequest("new");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("home");
        home.setAddress("addr");
        home.setOwnerId(1);
        RoomEntity room = new RoomEntity();
        room.setId(5);
        room.setName("room");
        room.setHome(home);
        Mockito.when(roomService.findById(roomId)).thenReturn(room);
        RoomEntity roomEdited = new RoomEntity();
        room.setId(5);
        room.setName("new");
        room.setHome(home);
        Mockito.when(roomService.edit(roomId, roomRequest)).thenReturn(roomEdited);
        RoomDetailed expected = new RoomDetailed(5, "new");
        Mockito.when(conversionService.convert(roomEdited, RoomDetailed.class)).thenReturn(expected);

        RoomDetailed actual = roomController.editRoom(roomId, roomRequest);

        assertEquals(expected, actual);
    }

    @Test
    void editRoom_shouldThrowException_whenUserIsNotOwner() {
        int roomId = 5;
        RoomRequest roomRequest = new RoomRequest("new");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(3);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("home");
        home.setAddress("addr");
        home.setOwnerId(1);
        RoomEntity room = new RoomEntity();
        room.setId(5);
        room.setName("room");
        room.setHome(home);
        Mockito.when(roomService.findById(roomId)).thenReturn(room);

        assertThrows(ResponseStatusException.class, () -> roomController.editRoom(roomId, roomRequest));
    }

    @Test
    void editRoom_shouldThrowException_whenRoomNotFound() {
        int roomId = 5;
        RoomRequest roomRequest = new RoomRequest("new");
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        Mockito.when(roomService.findById(roomId)).thenThrow(RoomNotFoundException.class);

        assertThrows(RoomNotFoundException.class, () -> roomController.editRoom(roomId, roomRequest));
    }

    @Test
    void deleteRoom() {
        int roomId = 5;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("home");
        home.setAddress("addr");
        home.setOwnerId(1);
        RoomEntity room = new RoomEntity();
        room.setId(5);
        room.setName("room");
        room.setHome(home);
        Mockito.when(roomService.findById(roomId)).thenReturn(room);

        ResponseEntity<HttpStatus> actual = roomController.deleteRoom(roomId);

        Mockito.verify(roomService).deleteById(roomId);
        assertTrue(actual.getStatusCode().is2xxSuccessful());
    }

    @Test
    void deleteRoom_shouldThrowException_whenUserIsNotOwner() {
        int roomId = 5;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(3);
        HomeEntity home = new HomeEntity();
        home.setId(10);
        home.setName("home");
        home.setAddress("addr");
        home.setOwnerId(1);
        RoomEntity room = new RoomEntity();
        room.setId(5);
        room.setName("room");
        room.setHome(home);
        Mockito.when(roomService.findById(roomId)).thenReturn(room);

        assertThrows(ResponseStatusException.class, () -> roomController.deleteRoom(roomId));

    }

    @Test
    void deleteRoom_shouldThrowException_whenRoomNotFound() {
        int roomId = 5;
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);
        Mockito.when(roomService.findById(roomId)).thenThrow(RoomNotFoundException.class);

        assertThrows(RoomNotFoundException.class, () -> roomController.deleteRoom(roomId));
    }
}