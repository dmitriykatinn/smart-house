package ru.handh.homeservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.homeservice.dto.response.RoomDetailed;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.entity.RoomEntity;

import static org.junit.jupiter.api.Assertions.*;

class RoomToRoomDetailedConverterTest {

    @Test
    void convert() {
        RoomToRoomDetailedConverter converter = new RoomToRoomDetailedConverter();
        RoomEntity room = new RoomEntity();
        room.setId(1);
        room.setName("room");
        room.setHome(new HomeEntity());
        RoomDetailed expected = new RoomDetailed(1, "room");

        RoomDetailed actual = converter.convert(room);

        assertEquals(expected, actual);
    }
}