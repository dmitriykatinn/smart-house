package ru.handh.homeservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.homeservice.dto.response.HomeSimple;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.entity.RoomEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HomeToHomeSimpleConverterTest {

    @Test
    void convert() {
        HomeToHomeSimpleConverter converter = new HomeToHomeSimpleConverter();
        HomeEntity home = new HomeEntity();
        home.setId(1);
        home.setAddress("address");
        home.setName("home");
        home.setOwnerId(10);
        home.setRooms(List.of(new RoomEntity("first"), new RoomEntity("second")));
        HomeSimple expected = new HomeSimple(1, "home");

        HomeSimple actual = converter.convert(home);

        assertEquals(expected, actual);
    }
}