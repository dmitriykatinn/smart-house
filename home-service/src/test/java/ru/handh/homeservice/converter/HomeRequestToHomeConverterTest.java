package ru.handh.homeservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.homeservice.dto.request.HomeRequest;
import ru.handh.homeservice.entity.HomeEntity;

import static org.junit.jupiter.api.Assertions.*;

class HomeRequestToHomeConverterTest {

    @Test
    void convert() {
        HomeRequestToHomeConverter converter = new HomeRequestToHomeConverter();
        HomeRequest homeRequest = new HomeRequest();
        homeRequest.setName("name");
        homeRequest.setAddress("address");
        HomeEntity expected = new HomeEntity();
        expected.setName("name");
        expected.setAddress("address");

        HomeEntity actual = converter.convert(homeRequest);

        assertEquals(expected, actual);
    }
}