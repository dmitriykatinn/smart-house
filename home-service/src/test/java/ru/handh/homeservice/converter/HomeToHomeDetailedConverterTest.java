package ru.handh.homeservice.converter;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.convert.ConversionService;
import ru.handh.homeservice.dto.response.HomeDetailed;
import ru.handh.homeservice.dto.response.RoomDetailed;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.entity.RoomEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HomeToHomeDetailedConverterTest {

    @Test
    void convert() {
        ConversionService conversionService = Mockito.mock(ConversionService.class);
        HomeToHomeDetailedConverter converter = new HomeToHomeDetailedConverter(conversionService);
        HomeEntity home = new HomeEntity();
        home.setId(1);
        home.setAddress("address");
        home.setName("home");
        home.setOwnerId(10);
        RoomDetailed roomDetailedFirst = new RoomDetailed(1, "first");
        RoomDetailed roomDetailedSecond = new RoomDetailed(2, "second");
        RoomEntity firstRoom = new RoomEntity("first");
        firstRoom.setId(1);
        firstRoom.setHome(home);
        Mockito.when(conversionService.convert(firstRoom, RoomDetailed.class)).thenReturn(roomDetailedFirst);
        RoomEntity secondRoom = new RoomEntity("second");
        secondRoom.setId(2);
        secondRoom.setHome(home);
        Mockito.when(conversionService.convert(secondRoom, RoomDetailed.class)).thenReturn(roomDetailedSecond);
        home.setRooms(List.of(firstRoom, secondRoom));
        HomeDetailed expected = new HomeDetailed();
        expected.setId(1);
        expected.setAddress("address");
        expected.setName("home");
        expected.setRooms(List.of(roomDetailedFirst, roomDetailedSecond));

        HomeDetailed actual = converter.convert(home);

        assertEquals(expected, actual);
    }
}