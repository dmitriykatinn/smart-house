package ru.handh.homeservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.homeservice.dto.request.RoomRequest;
import ru.handh.homeservice.entity.RoomEntity;

import static org.junit.jupiter.api.Assertions.*;

class RoomRequestToRoomConverterTest {

    @Test
    void convert() {
        RoomRequestToRoomConverter converter = new RoomRequestToRoomConverter();
        RoomRequest roomRequest = new RoomRequest("room");
        RoomEntity expected = new RoomEntity();
        expected.setName("room");

        RoomEntity actual = converter.convert(roomRequest);

        assertEquals(expected, actual);
    }
}