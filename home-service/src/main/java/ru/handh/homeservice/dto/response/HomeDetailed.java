package ru.handh.homeservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HomeDetailed {
    private int id;
    private String name;
    private String address;
    private List<RoomDetailed> rooms;
}
