package ru.handh.homeservice.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoomDetailed {
    private int id;
    private String name;
}
