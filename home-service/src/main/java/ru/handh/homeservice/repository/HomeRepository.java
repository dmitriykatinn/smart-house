package ru.handh.homeservice.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.handh.homeservice.entity.HomeEntity;

import java.util.Optional;

@Repository
public interface HomeRepository extends JpaRepository<HomeEntity, Integer> {
    @EntityGraph(attributePaths = {"rooms"})
    Optional<HomeEntity> findById(int id);

    void deleteByOwnerId(Integer ownerId);
}
