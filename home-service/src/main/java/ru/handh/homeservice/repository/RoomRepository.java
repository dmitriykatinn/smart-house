package ru.handh.homeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.handh.homeservice.entity.RoomEntity;

public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {
}
