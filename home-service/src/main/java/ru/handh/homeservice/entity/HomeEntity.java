package ru.handh.homeservice.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "homes")
@Data
@NoArgsConstructor
public class HomeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column
    private String name;

    @Column
    private String address;

    @NotNull
    @Column
    private int ownerId;

    @OneToMany(mappedBy = "home", cascade = CascadeType.ALL)
    private List<RoomEntity> rooms = Collections.emptyList();

    public HomeEntity(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
