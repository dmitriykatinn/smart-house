package ru.handh.homeservice.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.homeservice.dto.request.RoomRequest;
import ru.handh.homeservice.dto.response.RoomDetailed;
import ru.handh.homeservice.entity.RoomEntity;
import ru.handh.homeservice.service.HomeService;
import ru.handh.homeservice.service.RoomService;
import ru.handh.homeservice.utils.AuthorizationUtil;

@RestController
@RequestMapping("api/rooms")
@RequiredArgsConstructor
public class RoomController {

    private final RoomService roomService;
    private final HomeService homeService;
    private final ConversionService conversionService;
    private final AuthorizationUtil authorizationUtil;
    private final KafkaTemplate<String, Integer> kafkaTemplate;

    @PostMapping
    public RoomDetailed createRoom(@RequestParam int homeId,
                                   @RequestBody @Valid RoomRequest roomRequest) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        int ownerId = homeService.findById(homeId).getOwnerId();
        if (loggedInUserId != ownerId) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        RoomEntity roomCreated = roomService.create(roomRequest, homeId);
        return conversionService.convert(roomCreated, RoomDetailed.class);
    }

    @PutMapping("/{roomId}")
    public RoomDetailed editRoom(@PathVariable int roomId,
                                 @RequestBody @Valid RoomRequest roomRequest) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        RoomEntity room = roomService.findById(roomId);
        int ownerId = room.getHome().getOwnerId();
        if (loggedInUserId != ownerId) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        RoomEntity roomEdited = roomService.edit(roomId, roomRequest);
        return conversionService.convert(roomEdited, RoomDetailed.class);
    }

    @DeleteMapping("/{roomId}")
    public ResponseEntity<HttpStatus> deleteRoom(@PathVariable int roomId) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        RoomEntity room = roomService.findById(roomId);
        int ownerId = room.getHome().getOwnerId();
        if (loggedInUserId != ownerId) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        roomService.deleteById(roomId);
        kafkaTemplate.send("ROOM_DELETED", roomId);
        return ResponseEntity.ok().build();
    }
}
