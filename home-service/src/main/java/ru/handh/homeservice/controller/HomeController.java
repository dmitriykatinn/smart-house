package ru.handh.homeservice.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.homeservice.dto.request.HomeRequest;
import ru.handh.homeservice.dto.response.HomeDetailed;
import ru.handh.homeservice.dto.response.HomeSimple;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.service.HomeService;
import ru.handh.homeservice.utils.AuthorizationUtil;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/homes")
@RequiredArgsConstructor
public class HomeController {

    private final HomeService homeService;
    private final ConversionService conversionService;
    private final AuthorizationUtil authorizationUtil;
    private final KafkaTemplate<String, Integer> kafkaTemplate;

    @PostMapping
    public HomeDetailed createHome(@RequestBody @Valid HomeRequest homeRequest) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        HomeEntity homeCreated = homeService.create(homeRequest, loggedInUserId);
        return conversionService.convert(homeCreated, HomeDetailed.class);
    }

    @PutMapping("/{homeId}")
    public HomeDetailed editHome(@PathVariable int homeId,
                                 @RequestBody @Valid HomeRequest homeRequest) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        int ownerId = homeService.findById(homeId).getOwnerId();
        if (loggedInUserId != ownerId) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        HomeEntity homeEdited = homeService.edit(homeId, homeRequest);
        return conversionService.convert(homeEdited, HomeDetailed.class);
    }
    @GetMapping("/{homeId}")
    public HomeDetailed getHome(@PathVariable int homeId) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        HomeEntity home = homeService.findById(homeId);
        if (loggedInUserId != home.getOwnerId()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        return conversionService.convert(home, HomeDetailed.class);
    }

    @GetMapping
    public List<HomeSimple> getHomesList() {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        return homeService.findAll().stream()
                .filter(home -> home.getOwnerId() == loggedInUserId)
                .map(home -> conversionService.convert(home, HomeSimple.class))
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{homeId}")
    public ResponseEntity<HttpStatus> deleteHome(@PathVariable int homeId) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        HomeEntity home = homeService.findById(homeId);
        if (loggedInUserId != home.getOwnerId()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        homeService.deleteById(homeId);
        kafkaTemplate.send("HOME_DELETED", homeId);
        return ResponseEntity.ok().build();
    }
}
