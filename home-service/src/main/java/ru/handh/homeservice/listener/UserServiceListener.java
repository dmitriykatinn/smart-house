package ru.handh.homeservice.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.handh.homeservice.service.HomeService;

@Component
@Slf4j
@RequiredArgsConstructor
public class UserServiceListener {

    private final HomeService homeService;

    @KafkaListener(topics = "USER_DELETED", groupId = "test")
    public void deleteHomesByOwnerId(Integer ownerId) {
        homeService.deleteByOwnerId(ownerId);
        log.info("Delete homes owned by user with userId={}", ownerId);
    }
}
