package ru.handh.homeservice.service.impl;

import jakarta.persistence.Table;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import ru.handh.homeservice.dto.request.HomeRequest;
import ru.handh.homeservice.exception.HomeNotFoundException;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.repository.HomeRepository;
import ru.handh.homeservice.service.HomeService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HomeServiceImpl implements HomeService {

    private final HomeRepository homeRepository;
    private final ConversionService conversionService;

    @Override
    public HomeEntity create(HomeRequest homeRequest, int ownerId) {
        HomeEntity home = conversionService.convert(homeRequest, HomeEntity.class);
        home.setOwnerId(ownerId);
        return homeRepository.save(home);
    }

    @Override
    public HomeEntity edit(int homeId, HomeRequest homeRequest) {
        HomeEntity home = findById(homeId);
        home.setName(homeRequest.getName());
        home.setAddress(homeRequest.getAddress());
        return homeRepository.save(home);
    }

    @Override
    public HomeEntity findById(int homeId) {
        return homeRepository.findById(homeId).orElseThrow(HomeNotFoundException::new);
    }

    @Override
    public List<HomeEntity> findAll() {
        return homeRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteById(int homeId) {
        HomeEntity home = findById(homeId);
        homeRepository.delete(home);
    }

    @Override
    @Transactional
    public void deleteByOwnerId(Integer ownerId) {
        homeRepository.deleteByOwnerId(ownerId);
    }
}
