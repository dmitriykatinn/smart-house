package ru.handh.homeservice.service;

import ru.handh.homeservice.dto.request.HomeRequest;
import ru.handh.homeservice.entity.HomeEntity;

import java.util.List;

public interface HomeService {
    HomeEntity create(HomeRequest homeRequest, int ownerId);
    HomeEntity edit(int homeId, HomeRequest homeRequest);
    HomeEntity findById(int homeId);
    List<HomeEntity> findAll();
    void deleteById(int homeId);
    void deleteByOwnerId(Integer ownerId);
}
