package ru.handh.homeservice.service;

import ru.handh.homeservice.dto.request.RoomRequest;
import ru.handh.homeservice.entity.RoomEntity;

public interface RoomService {
    RoomEntity create(RoomRequest roomRequest, int homeId);
    RoomEntity edit(int roomId, RoomRequest roomRequest);
    void deleteById(int roomId);
    RoomEntity findById(int roomId);
}
