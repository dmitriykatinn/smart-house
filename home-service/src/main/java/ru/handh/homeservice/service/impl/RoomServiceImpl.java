package ru.handh.homeservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import ru.handh.homeservice.dto.request.RoomRequest;
import ru.handh.homeservice.exception.RoomNotFoundException;
import ru.handh.homeservice.entity.HomeEntity;
import ru.handh.homeservice.entity.RoomEntity;
import ru.handh.homeservice.repository.RoomRepository;
import ru.handh.homeservice.service.HomeService;
import ru.handh.homeservice.service.RoomService;

@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final HomeService homeService;
    private final RoomRepository roomRepository;
    private final ConversionService conversionService;

    @Override
    public RoomEntity create(RoomRequest roomRequest, int homeId) {
        HomeEntity home = homeService.findById(homeId);
        RoomEntity room = conversionService.convert(roomRequest, RoomEntity.class);
        room.setHome(home);
        roomRepository.save(room);
        return room;
    }

    @Override
    public RoomEntity edit(int roomId, RoomRequest roomRequest) {
        RoomEntity roomEdited = findById(roomId);
        roomEdited.setName(roomRequest.getName());
        return roomRepository.save(roomEdited);
    }

    @Override
    public void deleteById(int roomId) {
        RoomEntity room = findById(roomId);
        roomRepository.delete(room);
    }

    @Override
    public RoomEntity findById(int roomId) {
        return roomRepository.findById(roomId).orElseThrow(RoomNotFoundException::new);
    }
}
