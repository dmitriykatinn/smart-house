package ru.handh.homeservice.converter;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import ru.handh.homeservice.dto.response.HomeDetailed;
import ru.handh.homeservice.dto.response.RoomDetailed;
import ru.handh.homeservice.entity.HomeEntity;

import java.util.List;
import java.util.stream.Collectors;

public class HomeToHomeDetailedConverter implements Converter<HomeEntity, HomeDetailed> {

    private final ConversionService conversionService;

    public HomeToHomeDetailedConverter(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    public HomeDetailed convert(HomeEntity source) {
        List<RoomDetailed> rooms = source.getRooms().stream()
                .map(room -> conversionService.convert(room, RoomDetailed.class))
                .collect(Collectors.toList());
        return new HomeDetailed(source.getId(), source.getName(), source.getAddress(), rooms);
    }
}
