package ru.handh.homeservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.homeservice.dto.response.RoomDetailed;
import ru.handh.homeservice.entity.RoomEntity;

public class RoomToRoomDetailedConverter implements Converter<RoomEntity, RoomDetailed> {
    @Override
    public RoomDetailed convert(RoomEntity source) {
        return new RoomDetailed(source.getId(), source.getName());
    }
}
