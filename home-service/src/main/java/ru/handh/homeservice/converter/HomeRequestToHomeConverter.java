package ru.handh.homeservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.homeservice.dto.request.HomeRequest;
import ru.handh.homeservice.entity.HomeEntity;

public class HomeRequestToHomeConverter implements Converter<HomeRequest, HomeEntity> {

    @Override
    public HomeEntity convert(HomeRequest source) {
        return new HomeEntity(source.getName(), source.getAddress());
    }
}
