package ru.handh.homeservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.homeservice.dto.request.RoomRequest;
import ru.handh.homeservice.entity.RoomEntity;

public class RoomRequestToRoomConverter implements Converter<RoomRequest, RoomEntity> {
    @Override
    public RoomEntity convert(RoomRequest source) {
        return new RoomEntity(source.getName());
    }
}
