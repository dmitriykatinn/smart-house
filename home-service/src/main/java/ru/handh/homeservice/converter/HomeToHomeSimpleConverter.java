package ru.handh.homeservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.homeservice.dto.response.HomeSimple;
import ru.handh.homeservice.entity.HomeEntity;

public class HomeToHomeSimpleConverter implements Converter<HomeEntity, HomeSimple> {

    @Override
    public HomeSimple convert(HomeEntity source) {
        return new HomeSimple(source.getId(), source.getName());
    }
}
