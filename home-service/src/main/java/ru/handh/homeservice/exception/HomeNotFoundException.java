package ru.handh.homeservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class HomeNotFoundException extends RuntimeException {
    public HomeNotFoundException() {
        super("Home with this id wasn't found!");
    }
}
