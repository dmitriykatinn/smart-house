package ru.handh.homeservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ru.handh.homeservice.converter.*;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    private final ConversionService conversionService;

    public WebConfig(@Lazy ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new HomeRequestToHomeConverter());
        registry.addConverter(new HomeToHomeSimpleConverter());
        registry.addConverter(new HomeToHomeDetailedConverter(conversionService));
        registry.addConverter(new RoomRequestToRoomConverter());
        registry.addConverter(new RoomToRoomDetailedConverter());
    }
}
