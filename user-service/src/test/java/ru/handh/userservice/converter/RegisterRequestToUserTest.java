package ru.handh.userservice.converter;

import org.junit.jupiter.api.Test;
import ru.handh.userservice.dto.request.RegisterRequest;
import ru.handh.userservice.entity.UserEntity;

import static org.junit.jupiter.api.Assertions.*;

class RegisterRequestToUserTest {

    @Test
    void convert() {
        RegisterRequestToUser converter = new RegisterRequestToUser();
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setName("name");
        registerRequest.setUsername("user");
        registerRequest.setPassword("qwerty");
        registerRequest.setConfirmPassword("qwerty");
        UserEntity expected = new UserEntity();
        expected.setName("name");
        expected.setUsername("user");
        expected.setPassword("qwerty");

        UserEntity actual = converter.convert(registerRequest);

        assertEquals(expected, actual);
    }
}