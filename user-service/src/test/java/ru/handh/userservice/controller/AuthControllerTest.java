package ru.handh.userservice.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.handh.userservice.dto.request.AuthRequest;
import ru.handh.userservice.dto.request.RefreshRequest;
import ru.handh.userservice.dto.request.RegisterRequest;
import ru.handh.userservice.dto.response.TokenResponse;
import ru.handh.userservice.entity.RefreshTokenEntity;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.service.RefreshTokenService;
import ru.handh.userservice.service.UserService;
import ru.handh.userservice.util.AuthorizationUtil;
import ru.handh.userservice.util.JwtUtil;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AuthControllerTest {

    @InjectMocks
    private AuthController authController;
    @Mock
    private UserService userService;
    @Mock
    private RefreshTokenService refreshTokenService;
    @Mock
    private JwtUtil jwtUtil;
    @Mock
    private AuthorizationUtil authorizationUtil;

    @Test
    void register() {
        RegisterRequest registerRequest = new RegisterRequest();
        UserEntity user = new UserEntity();
        Mockito.when(userService.register(registerRequest)).thenReturn(user);
        RefreshTokenEntity refreshToken = new RefreshTokenEntity();
        UUID uuid = UUID.fromString("73cc2c76-225f-403a-9d4a-424106ef2ab4");
        refreshToken.setToken(uuid);
        Mockito.when(refreshTokenService.generateAndSaveToken(user)).thenReturn(refreshToken);
        String accessToken = "access";
        Mockito.when(jwtUtil.generateToken(user)).thenReturn(accessToken);
        Mockito.when(jwtUtil.getTtl()).thenReturn(60L);
        TokenResponse expected = new TokenResponse(accessToken, uuid, 60L);

        TokenResponse actual = authController.register(registerRequest);

        assertEquals(expected, actual);
    }

    @Test
    void authenticate() {
        AuthRequest authRequest = new AuthRequest("user", "qwerty");
        UserEntity user = new UserEntity();
        Mockito.when(userService.authenticate(authRequest)).thenReturn(user);
        RefreshTokenEntity refreshToken = new RefreshTokenEntity();
        UUID uuid = UUID.fromString("73cc2c76-225f-403a-9d4a-424106ef2ab4");
        refreshToken.setToken(uuid);
        Mockito.when(refreshTokenService.generateAndSaveToken(user)).thenReturn(refreshToken);
        String accessToken = "access";
        Mockito.when(jwtUtil.generateToken(user)).thenReturn(accessToken);
        Mockito.when(jwtUtil.getTtl()).thenReturn(60L);
        TokenResponse expected = new TokenResponse(accessToken, uuid, 60L);

        TokenResponse actual = authController.authenticate(authRequest);

        assertEquals(expected, actual);
    }

    @Test
    void refresh() {
        RefreshRequest refreshRequest = new RefreshRequest();
        refreshRequest.setRefreshToken(UUID.fromString("73cc2c76-225f-403a-9d4a-424106ef2ab4"));
        UserEntity user = new UserEntity();
        RefreshTokenEntity refreshToken = new RefreshTokenEntity();
        UUID uuid = UUID.fromString("73cc2c76-225f-403a-9d4a-424106ef2ab4");
        refreshToken.setToken(uuid);
        refreshToken.setUser(user);
        Mockito.when(refreshTokenService.refresh(refreshRequest.getRefreshToken())).thenReturn(refreshToken);
        String accessToken = "access";
        Mockito.when(jwtUtil.generateToken(user)).thenReturn(accessToken);
        Mockito.when(jwtUtil.getTtl()).thenReturn(60L);
        TokenResponse expected = new TokenResponse(accessToken, uuid, 60L);

        TokenResponse actual = authController.refresh(refreshRequest);

        assertEquals(expected, actual);
    }

    @Test
    void signout() {
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);

        ResponseEntity<HttpStatus> actual = authController.signout();

        Mockito.verify(refreshTokenService).deleteByUserId(1);
        assertTrue(actual.getStatusCode().is2xxSuccessful());
    }
}