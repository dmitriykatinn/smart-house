package ru.handh.userservice.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import ru.handh.userservice.dto.request.DeleteUserRequest;
import ru.handh.userservice.service.UserService;
import ru.handh.userservice.util.AuthorizationUtil;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @InjectMocks
    private UserController userController;
    @Mock
    private UserService userService;
    @Mock
    private AuthorizationUtil authorizationUtil;
    @Mock
    private KafkaTemplate<String, Integer> kafkaTemplate;

    @Test
    void deleteUser() {
        DeleteUserRequest deleteUserRequest = new DeleteUserRequest();
        Mockito.when(authorizationUtil.getLoggedInUserId()).thenReturn(1);

        ResponseEntity<HttpStatus> actual = userController.deleteUser(deleteUserRequest);

        Mockito.verify(userService).delete(1, deleteUserRequest);
        assertTrue(actual.getStatusCode().is2xxSuccessful());
    }
}