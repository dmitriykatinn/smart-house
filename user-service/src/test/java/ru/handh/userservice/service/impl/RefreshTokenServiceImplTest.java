package ru.handh.userservice.service.impl;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.handh.userservice.entity.RefreshTokenEntity;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.repository.RefreshTokenRepository;
import ru.handh.userservice.util.TimeUtil;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RefreshTokenServiceImplTest {

    @InjectMocks
    private RefreshTokenServiceImpl refreshTokenService;
    @Mock
    private RefreshTokenRepository refreshTokenRepository;
    @Mock
    private TimeUtil timeUtil;
    private Long ttl = 10L;

    @BeforeEach
    void init() {
        refreshTokenService.setTtl(ttl);
    }

    @Test
    void generateAndSaveToken() {
        UserEntity user = new UserEntity();
        Mockito.when(timeUtil.getCurrentTime()).thenReturn(Instant.ofEpochSecond(100));
        RefreshTokenEntity refreshToken = new RefreshTokenEntity();
        refreshToken.setUser(user);
        refreshToken.setExpiryDate(Instant.ofEpochSecond(100 + ttl));
        Mockito.when(refreshTokenRepository.save(refreshToken)).thenReturn(refreshToken);

        RefreshTokenEntity actual = refreshTokenService.generateAndSaveToken(user);

        assertEquals(refreshToken, actual);
    }

    @Test
    void refresh() {
        UUID uuid = UUID.fromString("8f456766-5aab-48b5-992f-ba8ba347d220");
        RefreshTokenEntity token = new RefreshTokenEntity();
        token.setToken(uuid);
        UserEntity user = new UserEntity();
        token.setUser(user);
        token.setExpiryDate(Instant.ofEpochSecond(100));
        Mockito.when(timeUtil.getCurrentTime()).thenReturn(Instant.ofEpochSecond(99));
        Mockito.when(refreshTokenRepository.findById(uuid)).thenReturn(Optional.of(token));
        RefreshTokenEntity expected = new RefreshTokenEntity(user, Instant.ofEpochSecond(109));
        Mockito.when(refreshTokenRepository.save(expected)).thenReturn(expected);

        RefreshTokenEntity actual = refreshTokenService.refresh(uuid);

        assertEquals(expected, actual);
    }

    @Test
    void refresh_shouldThrowException_whenTokenExpired() {
        UUID uuid = UUID.fromString("8f456766-5aab-48b5-992f-ba8ba347d220");
        RefreshTokenEntity token = new RefreshTokenEntity();
        token.setToken(uuid);
        UserEntity user = new UserEntity();
        token.setUser(user);
        token.setExpiryDate(Instant.ofEpochSecond(100));
        Mockito.when(timeUtil.getCurrentTime()).thenReturn(Instant.ofEpochSecond(101));
        Mockito.when(refreshTokenRepository.findById(uuid)).thenReturn(Optional.of(token));

        assertThrows(Exception.class, () -> refreshTokenService.refresh(uuid));
    }

    @Test
    void refresh_shouldThrowException_whenTokenNotFound() {
        UUID uuid = UUID.fromString("8f456766-5aab-48b5-992f-ba8ba347d220");
        Mockito.when(refreshTokenRepository.findById(uuid)).thenReturn(Optional.empty());

        assertThrows(Exception.class, () -> refreshTokenService.refresh(uuid));
    }

    @Test
    void deleteByUserId() {
        int userId = 5;

        refreshTokenService.deleteByUserId(userId);

        Mockito.verify(refreshTokenRepository).deleteByUserId(5);
    }
}