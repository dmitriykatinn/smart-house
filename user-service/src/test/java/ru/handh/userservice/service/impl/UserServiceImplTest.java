package ru.handh.userservice.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.userservice.dto.request.AuthRequest;
import ru.handh.userservice.dto.request.DeleteUserRequest;
import ru.handh.userservice.dto.request.RegisterRequest;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.repository.UserRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private ConversionService conversionService;
    @Mock
    private PasswordEncoder passwordEncoder;

    @Test
    void findById() {
        int userId = 5;
        UserEntity expected = new UserEntity();
        expected.setId(userId);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(expected));

        UserEntity actual = userService.findById(userId);

        assertEquals(expected, actual);
    }

    @Test
    void findById_shouldThrowException_whenUserNotFound() {
        int userId = 5;
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> userService.findById(userId));
    }

    @Test
    void register() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("user");
        registerRequest.setPassword("qwerty");
        registerRequest.setConfirmPassword("qwerty");
        Mockito.when(userRepository.findByUsername("user")).thenReturn(Optional.empty());
        Mockito.when(passwordEncoder.encode("qwerty")).thenReturn("encoded_qwerty");
        RegisterRequest registerRequestEncoded = new RegisterRequest();
        registerRequestEncoded.setUsername("user");
        registerRequestEncoded.setPassword("encoded_qwerty");
        registerRequestEncoded.setConfirmPassword("qwerty");
        UserEntity expected = new UserEntity();
        expected.setUsername("user");
        expected.setPassword("encoded_qwerty");
        Mockito.when(conversionService.convert(registerRequestEncoded, UserEntity.class)).thenReturn(expected);
        Mockito.when(userRepository.save(expected)).thenReturn(expected);

        UserEntity actual = userService.register(registerRequest);

        assertEquals(expected, actual);

        UserEntity user = new UserEntity();
        user.setUsername("user");
        user.setPassword("qwerty");
    }

    @Test
    void register_shouldThrowException_whenPasswordAndConfirmPasswordDoesntMatch() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("user");
        registerRequest.setPassword("qwerty");
        registerRequest.setConfirmPassword("qwerty!");

        assertThrows(ResponseStatusException.class, () -> userService.register(registerRequest));
    }

    @Test
    void register_shouldThrowException_whenUserExists() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("user");
        registerRequest.setPassword("qwerty");
        registerRequest.setConfirmPassword("qwerty");
        UserEntity user = new UserEntity();
        user.setUsername("user");
        user.setPassword("qwerty");
        Mockito.when(userRepository.findByUsername("user")).thenReturn(Optional.of(user));

        assertThrows(ResponseStatusException.class, () -> userService.register(registerRequest));
    }

    @Test
    void authenticate() {
        AuthRequest authRequest = new AuthRequest("user", "qwerty");
        UserEntity expected = new UserEntity();
        expected.setUsername("user");
        expected.setPassword("encoded_qwerty");
        Mockito.when(userRepository.findByUsername("user")).thenReturn(Optional.of(expected));
        Mockito.when(passwordEncoder.matches("qwerty", "encoded_qwerty")).thenReturn(true);

        UserEntity actual = userService.authenticate(authRequest);

        assertEquals(expected, actual);
    }

    @Test
    void authenticate_shouldThrowException_whenUserNotExists() {
        AuthRequest authRequest = new AuthRequest("user", "qwerty");
        Mockito.when(userRepository.findByUsername("user")).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> userService.authenticate(authRequest));
    }

    @Test
    void authenticate_shouldThrowException_whenPasswordWrong() {
        AuthRequest authRequest = new AuthRequest("user", "qwerty!!!");
        UserEntity expected = new UserEntity();
        expected.setUsername("user");
        expected.setPassword("encoded_qwerty");
        Mockito.when(userRepository.findByUsername("user")).thenReturn(Optional.of(expected));
        Mockito.when(passwordEncoder.matches("qwerty!!!", "encoded_qwerty")).thenReturn(false);

        assertThrows(ResponseStatusException.class, () -> userService.authenticate(authRequest));

    }

    @Test
    void delete() {
        int userId = 5;
        DeleteUserRequest deleteUserRequest = new DeleteUserRequest();
        deleteUserRequest.setPassword("qwerty");
        UserEntity user = new UserEntity();
        user.setId(userId);
        user.setPassword("encoded_qwerty");
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches("qwerty", "encoded_qwerty")).thenReturn(true);

        userService.delete(userId, deleteUserRequest);

        Mockito.verify(userRepository).delete(user);
    }

    @Test
    void delete_shouldThrowException_whenUserNotFound() {
        int userId = 5;
        DeleteUserRequest deleteUserRequest = new DeleteUserRequest();
        deleteUserRequest.setPassword("qwerty");
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(ResponseStatusException.class, () -> userService.delete(userId, deleteUserRequest));
    }

    @Test
    void delete_shouldThrowException_whenPasswordDoesntMatch() {
        int userId = 5;
        DeleteUserRequest deleteUserRequest = new DeleteUserRequest();
        deleteUserRequest.setPassword("qwe");
        UserEntity user = new UserEntity();
        user.setId(userId);
        user.setPassword("encoded_qwerty");
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches("qwe", "encoded_qwerty")).thenReturn(false);

        assertThrows(ResponseStatusException.class, () -> userService.delete(userId, deleteUserRequest));
    }
}