package ru.handh.userservice.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Data
public class RefreshRequest {
    @NotNull
    private UUID refreshToken;
}
