package ru.handh.userservice.dto.response;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class TokenResponse {
    @NotBlank
    private String accessToken;
    @NotBlank
    private UUID refreshToken;
    @NotNull
    private Long ttl;
}
