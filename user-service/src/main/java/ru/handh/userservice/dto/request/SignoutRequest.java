package ru.handh.userservice.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class SignoutRequest {
    @NotBlank
    private String accessToken;
}
