package ru.handh.userservice.scheduler;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.handh.userservice.repository.RefreshTokenRepository;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

@EnableScheduling
@Slf4j
@Component
@RequiredArgsConstructor
public class Scheduler {

    private final RefreshTokenRepository refreshTokenRepository;

    @Scheduled(fixedDelay = 60, timeUnit = TimeUnit.DAYS)
    @Transactional
    public void deleteExpiredRefreshTokensOnceDay(){
        refreshTokenRepository.deleteByExpiryDateLessThan(Instant.now());
        log.info("Delete expired refresh tokens from database");
    }
}
