package ru.handh.userservice.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.handh.userservice.dto.request.*;
import ru.handh.userservice.dto.response.TokenResponse;
import ru.handh.userservice.entity.RefreshTokenEntity;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.service.RefreshTokenService;
import ru.handh.userservice.service.UserService;
import ru.handh.userservice.util.AuthorizationUtil;
import ru.handh.userservice.util.JwtUtil;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AuthController {

    private final UserService userService;
    private final RefreshTokenService refreshTokenService;
    private final JwtUtil jwtUtil;
    private final AuthorizationUtil authorizationUtil;

    @PostMapping("/register")
    public TokenResponse register(@RequestBody @Valid RegisterRequest registerRequest) {
        UserEntity user = userService.register(registerRequest);
        RefreshTokenEntity refreshToken = refreshTokenService.generateAndSaveToken(user);
        String accessToken = jwtUtil.generateToken(user);
        return new TokenResponse(accessToken, refreshToken.getToken(), jwtUtil.getTtl());
    }

    @PostMapping("/auth")
    public TokenResponse authenticate(@RequestBody @Valid AuthRequest authRequest) {
        UserEntity user = userService.authenticate(authRequest);
        RefreshTokenEntity refreshToken = refreshTokenService.generateAndSaveToken(user);
        String accessToken = jwtUtil.generateToken(user);
        return new TokenResponse(accessToken, refreshToken.getToken(), jwtUtil.getTtl());
    }

    @PostMapping("/refresh")
    public TokenResponse refresh(@RequestBody @Valid RefreshRequest refreshRequest) {
        RefreshTokenEntity refreshToken = refreshTokenService.refresh(refreshRequest.getRefreshToken());
        String accessToken = jwtUtil.generateToken(refreshToken.getUser());
        return new TokenResponse(accessToken, refreshToken.getToken(), jwtUtil.getTtl());
    }

    @PostMapping("/signout")
    public ResponseEntity<HttpStatus> signout() {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        refreshTokenService.deleteByUserId(loggedInUserId);
        return ResponseEntity.ok().build();
    }
}
