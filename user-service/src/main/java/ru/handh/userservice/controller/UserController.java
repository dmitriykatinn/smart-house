package ru.handh.userservice.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.handh.userservice.dto.request.DeleteUserRequest;
import ru.handh.userservice.service.UserService;
import ru.handh.userservice.util.AuthorizationUtil;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final AuthorizationUtil authorizationUtil;
    private final KafkaTemplate<String, Integer> kafkaTemplate;

    @DeleteMapping("/account")
    public ResponseEntity<HttpStatus> deleteUser(@RequestBody @Valid DeleteUserRequest request) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        userService.delete(loggedInUserId, request);
        kafkaTemplate.send("USER_DELETED", loggedInUserId);
        return ResponseEntity.ok().build();
    }
}
