package ru.handh.userservice.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.userservice.entity.RefreshTokenEntity;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.repository.RefreshTokenRepository;
import ru.handh.userservice.service.RefreshTokenService;
import ru.handh.userservice.util.TimeUtil;

import java.time.Instant;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RefreshTokenServiceImpl implements RefreshTokenService {

    private final RefreshTokenRepository refreshTokenRepository;
    private final TimeUtil timeUtil;

    @Value("${jwt.refresh-token.ttl}")
    private Long ttl;

    @Override
    @Transactional
    public RefreshTokenEntity generateAndSaveToken(UserEntity user) {
        refreshTokenRepository.deleteByUser(user);
        refreshTokenRepository.flush();
        Instant expiryDate = timeUtil.getCurrentTime().plusSeconds(ttl);
        return refreshTokenRepository.save(new RefreshTokenEntity(user, expiryDate));
    }

    @Override
    @Transactional
    public RefreshTokenEntity refresh(UUID refreshToken) {
        RefreshTokenEntity token = refreshTokenRepository.findById(refreshToken)
                .orElseThrow(()-> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Refresh token is not valid"));
        verifyExpiration(token);
        UserEntity user = token.getUser();
        refreshTokenRepository.delete(token);
        refreshTokenRepository.flush();
        Instant expiryDate = timeUtil.getCurrentTime().plusSeconds(ttl);
        return refreshTokenRepository.save(new RefreshTokenEntity(user, expiryDate));
    }

    @Override
    @Transactional
    public void deleteByUserId(int userId) {
        refreshTokenRepository.deleteByUserId(userId);
    }

    private void verifyExpiration(RefreshTokenEntity refreshToken) {
        Instant expiryDate = refreshToken.getExpiryDate();
        if (expiryDate.compareTo(timeUtil.getCurrentTime()) < 0) {
            refreshTokenRepository.delete(refreshToken);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Refresh token is expired");
        }
    }

    public void setTtl(Long ttl) {
        this.ttl = ttl;
    }
}
