package ru.handh.userservice.service;

import ru.handh.userservice.dto.request.AuthRequest;
import ru.handh.userservice.dto.request.DeleteUserRequest;
import ru.handh.userservice.dto.request.RegisterRequest;
import ru.handh.userservice.entity.UserEntity;

public interface UserService {
    UserEntity register(RegisterRequest registerRequest);
    UserEntity authenticate(AuthRequest authRequest);
    UserEntity findById(int userId);
    void delete(int userId, DeleteUserRequest request);
}
