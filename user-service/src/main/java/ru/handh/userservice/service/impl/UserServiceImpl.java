package ru.handh.userservice.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.userservice.dto.request.AuthRequest;
import ru.handh.userservice.dto.request.DeleteUserRequest;
import ru.handh.userservice.dto.request.RegisterRequest;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.repository.UserRepository;
import ru.handh.userservice.service.UserService;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ConversionService conversionService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserEntity findById(int userId) {
        return userRepository.findById(userId)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with this username doesn't exist"));
    }

    @Override
    public UserEntity register(RegisterRequest registerRequest) {
        String password = registerRequest.getPassword();
        String confirmPassword = registerRequest.getConfirmPassword();
        if (!password.equals(confirmPassword)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password and confirm password doesn't match");
        }
        if (userRepository.findByUsername(registerRequest.getUsername()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with the same username already exists");
        }
        String passwordEncoded = passwordEncoder.encode(registerRequest.getPassword());
        registerRequest.setPassword(passwordEncoded);
        UserEntity user = conversionService.convert(registerRequest, UserEntity.class);
        return userRepository.save(user);
    }

    @Override
    public UserEntity authenticate(AuthRequest authRequest) {
        UserEntity user = userRepository.findByUsername(authRequest.getUsername()).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with this username doesn't exist"));
        String userPasswordEncoded = user.getPassword();
        String authRequestPassword = authRequest.getPassword();
        if (passwordEncoder.matches(authRequestPassword, userPasswordEncoded)) {
            return user;
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password is wrong");
    }

    @Override
    @Transactional
    public void delete(int userId, DeleteUserRequest request) {
        UserEntity user = findById(userId);
        if (!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password is wrong");
        }
        userRepository.delete(user);
    }
}
