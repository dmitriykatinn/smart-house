package ru.handh.userservice.service;

import ru.handh.userservice.entity.RefreshTokenEntity;
import ru.handh.userservice.entity.UserEntity;

import java.util.UUID;

public interface RefreshTokenService {
    RefreshTokenEntity generateAndSaveToken(UserEntity user);
    RefreshTokenEntity refresh(UUID refreshToken);
    void deleteByUserId(int userId);
}
