package ru.handh.userservice.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "refresh_tokens")
@Data
@NoArgsConstructor
public class RefreshTokenEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID token;

    @OneToOne
    @JoinColumn(nullable = false)
    private UserEntity user;

    @Column
    @NotNull
    private Instant expiryDate;

    public RefreshTokenEntity(UserEntity user, Instant expiryDate) {
        this.user = user;
        this.expiryDate = expiryDate;
    }
}
