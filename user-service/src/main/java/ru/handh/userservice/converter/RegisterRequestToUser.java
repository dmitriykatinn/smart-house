package ru.handh.userservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.userservice.dto.request.RegisterRequest;
import ru.handh.userservice.entity.UserEntity;

public class RegisterRequestToUser implements Converter<RegisterRequest, UserEntity> {
    @Override
    public UserEntity convert(RegisterRequest source) {
        return new UserEntity(source.getUsername(), source.getName(), source.getPassword());
    }
}
