package ru.handh.userservice.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.handh.userservice.entity.UserEntity;
import ru.handh.userservice.service.UserService;

import java.time.*;

@Component
public class JwtUtil {

    @Value("${jwt.secret}")
    private String secret;
    @Getter
    @Value("${jwt.access-token.ttl}")
    private Long ttl;

    public String generateToken(UserEntity user) {
        return JWT.create()
                .withClaim("userId", user.getId())
                .withExpiresAt(Instant.now().plusSeconds(ttl))
                .sign(Algorithm.HMAC256(secret));
    }
}
