package ru.handh.deviceservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.handh.deviceservice.entity.DeviceEntity;

import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<DeviceEntity, Integer> {
    void deleteByOwnerId(Integer ownerId);

    void deleteByHomeId(Integer homeId);

    List<DeviceEntity> findByRoomId(Integer roomId);
}
