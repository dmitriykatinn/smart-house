package ru.handh.deviceservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.handh.deviceservice.entity.CapabilityEntity;

@Repository
public interface CapabilityRepository extends JpaRepository<CapabilityEntity, Integer> {
}
