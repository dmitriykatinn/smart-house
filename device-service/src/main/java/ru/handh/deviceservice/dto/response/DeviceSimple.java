package ru.handh.deviceservice.dto.response;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import ru.handh.deviceservice.entity.DeviceCategory;

@Data
public class DeviceSimple {

    @NotNull
    private Integer id;

    @NotBlank
    private String name;

    @NotNull
    private DeviceCategory category;
}
