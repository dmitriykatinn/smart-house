package ru.handh.deviceservice.dto.response;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import ru.handh.deviceservice.entity.CapabilityCode;

@Data
public class CapabilitySimple {

    @NotNull
    private CapabilityCode code;

    private String value;
}
