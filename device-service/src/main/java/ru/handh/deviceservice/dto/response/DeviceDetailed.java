package ru.handh.deviceservice.dto.response;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import ru.handh.deviceservice.entity.DeviceCategory;

import java.util.List;

@Data
public class DeviceDetailed {

    @NotNull
    private Integer id;

    @NotBlank
    private String name;

    @NotNull
    private DeviceCategory category;

    @NotNull
    private List<CapabilitySimple> capabilities;
}
