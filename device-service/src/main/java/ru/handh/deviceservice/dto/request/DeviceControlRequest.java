package ru.handh.deviceservice.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import ru.handh.deviceservice.dto.response.CapabilitySimple;

import java.util.List;

@Data
public class DeviceControlRequest {

    @NotNull
    private List<CapabilitySimple> capabilities;
}
