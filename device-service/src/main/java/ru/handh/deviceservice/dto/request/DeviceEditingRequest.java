package ru.handh.deviceservice.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class DeviceEditingRequest {

    @NotBlank
    private String name;

    @NotNull
    private Integer homeId;

    private Integer roomId;
}
