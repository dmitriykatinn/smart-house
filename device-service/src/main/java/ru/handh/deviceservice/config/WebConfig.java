package ru.handh.deviceservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ru.handh.deviceservice.converter.*;
import ru.handh.deviceservice.util.AuthorizationUtil;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    private final ConversionService conversionService;

    public WebConfig(@Lazy ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new CapabilityEntityToCapabilitySimpleConverter());
        registry.addConverter(new DeviceCreationToDeviceEntityConverter());
        registry.addConverter(new DeviceEntityToDeviceDetailedConverter(conversionService));
        registry.addConverter(new DeviceEntityToDeviceSimpleConverter());
        registry.addConverter(new CapabilitySimpleToCapabilityEntityConverter());
    }
}
