package ru.handh.deviceservice.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.deviceservice.dto.request.DeviceControlRequest;
import ru.handh.deviceservice.dto.request.DeviceCreationRequest;
import ru.handh.deviceservice.dto.request.DeviceEditingRequest;
import ru.handh.deviceservice.dto.response.DeviceDetailed;
import ru.handh.deviceservice.dto.response.DeviceSimple;
import ru.handh.deviceservice.entity.DeviceEntity;
import ru.handh.deviceservice.service.CapabilityService;
import ru.handh.deviceservice.service.DeviceService;
import ru.handh.deviceservice.util.AuthorizationUtil;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/devices")
@RequiredArgsConstructor
public class DeviceController {

    private final DeviceService deviceService;
    private final ConversionService conversionService;
    private final AuthorizationUtil authorizationUtil;
    private final CapabilityService capabilityService;

    @PostMapping
    public DeviceDetailed createDevice(@RequestBody @Valid DeviceCreationRequest request) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        DeviceEntity device = deviceService.createDevice(request, loggedInUserId);
        return conversionService.convert(device, DeviceDetailed.class);
    }

    @PutMapping("/{deviceId}")
    public DeviceDetailed editDevice(@PathVariable int deviceId,
                                     @RequestBody @Valid DeviceEditingRequest request) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        int ownerId = deviceService.findById(deviceId).getOwnerId();
        if (loggedInUserId != ownerId) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        DeviceEntity deviceEdited = deviceService.edit(deviceId, request);
        return conversionService.convert(deviceEdited, DeviceDetailed.class);
    }

    @GetMapping("/{deviceId}")
    public DeviceDetailed getDevice(@PathVariable int deviceId) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        DeviceEntity device = deviceService.findById(deviceId);
        if (loggedInUserId != device.getOwnerId()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        return conversionService.convert(device, DeviceDetailed.class);
    }

    @GetMapping
    public List<DeviceSimple> getDevicesList(@RequestParam Integer homeId,
                                             @RequestParam(required = false) Integer roomId) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        return deviceService.findAll().stream()
                .filter(device -> device.getOwnerId() == loggedInUserId)
                .filter(device -> Objects.equals(device.getHomeId(), homeId))
                .filter(device -> roomId == null || roomId.equals(device.getRoomId()))
                .map(device -> conversionService.convert(device, DeviceSimple.class))
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{deviceId}")
    public void deleteDevice(@PathVariable int deviceId) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        DeviceEntity device = deviceService.findById(deviceId);
        if (loggedInUserId != device.getOwnerId()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        deviceService.delete(device);
    }

    @PostMapping("/{deviceId}/control")
    public void controlDevice(@PathVariable int deviceId,
                              @RequestBody @Valid DeviceControlRequest request) {
        int loggedInUserId = authorizationUtil.getLoggedInUserId();
        DeviceEntity device = deviceService.findById(deviceId);
        if (loggedInUserId != device.getOwnerId()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        capabilityService.create(request.getCapabilities(), device);
    }
}
