package ru.handh.deviceservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.deviceservice.dto.response.DeviceSimple;
import ru.handh.deviceservice.entity.DeviceEntity;

public class DeviceEntityToDeviceSimpleConverter implements Converter<DeviceEntity, DeviceSimple> {
    @Override
    public DeviceSimple convert(DeviceEntity source) {
        DeviceSimple device = new DeviceSimple();
        device.setId(source.getId());
        device.setName(source.getName());
        device.setCategory(source.getCategory());
        return device;
    }
}
