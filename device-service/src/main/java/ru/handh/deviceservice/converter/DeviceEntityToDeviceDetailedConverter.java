package ru.handh.deviceservice.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import ru.handh.deviceservice.dto.response.CapabilitySimple;
import ru.handh.deviceservice.dto.response.DeviceDetailed;
import ru.handh.deviceservice.entity.DeviceEntity;

import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DeviceEntityToDeviceDetailedConverter implements Converter<DeviceEntity, DeviceDetailed> {

    private final ConversionService conversionService;

    @Override
    public DeviceDetailed convert(DeviceEntity source) {
        DeviceDetailed device = new DeviceDetailed();
        device.setId(source.getId());
        device.setName(source.getName());
        device.setCategory(source.getCategory());
        device.setCapabilities(source.getCapabilities().stream()
                .map(c -> conversionService.convert(c, CapabilitySimple.class))
                .collect(Collectors.toList())
        );
        return device;
    }
}
