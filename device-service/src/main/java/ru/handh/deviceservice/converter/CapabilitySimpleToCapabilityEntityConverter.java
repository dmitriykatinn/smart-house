package ru.handh.deviceservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.deviceservice.dto.response.CapabilitySimple;
import ru.handh.deviceservice.entity.CapabilityEntity;

public class CapabilitySimpleToCapabilityEntityConverter implements Converter<CapabilitySimple, CapabilityEntity> {
    @Override
    public CapabilityEntity convert(CapabilitySimple source) {
        CapabilityEntity capability = new CapabilityEntity();
        capability.setCode(source.getCode());
        capability.setValue(source.getValue());
        return capability;
    }
}
