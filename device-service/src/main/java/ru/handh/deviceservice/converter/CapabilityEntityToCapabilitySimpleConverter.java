package ru.handh.deviceservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.deviceservice.dto.response.CapabilitySimple;
import ru.handh.deviceservice.entity.CapabilityEntity;

public class CapabilityEntityToCapabilitySimpleConverter implements Converter<CapabilityEntity, CapabilitySimple> {
    @Override
    public CapabilitySimple convert(CapabilityEntity source) {
        CapabilitySimple capability = new CapabilitySimple();
        capability.setCode(source.getCode());
        capability.setValue(source.getValue());
        return capability;
    }
}
