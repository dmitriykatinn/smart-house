package ru.handh.deviceservice.converter;

import org.springframework.core.convert.converter.Converter;
import ru.handh.deviceservice.dto.request.DeviceCreationRequest;
import ru.handh.deviceservice.entity.DeviceEntity;

public class DeviceCreationToDeviceEntityConverter implements Converter<DeviceCreationRequest, DeviceEntity> {
    @Override
    public DeviceEntity convert(DeviceCreationRequest source) {
        DeviceEntity device = new DeviceEntity();
        device.setTuyaDeviceId(source.getTuyaDeviceId());
        device.setName(source.getName());
        device.setHomeId(source.getHomeId());
        device.setRoomId(source.getRoomId());
        return device;
    }
}
