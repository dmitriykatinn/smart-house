package ru.handh.deviceservice.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.handh.deviceservice.service.DeviceService;

@Component
@Slf4j
@RequiredArgsConstructor
public class UserServiceListener {

    private final DeviceService deviceService;

    @KafkaListener(topics = "USER_DELETED", groupId = "test")
    public void deleteDevicesByOwnerId(Integer ownerId) {
        deviceService.deleteByOwnerId(ownerId);
        log.info("Delete devices owned by user with userId={}", ownerId);
    }
}
