package ru.handh.deviceservice.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.handh.deviceservice.service.DeviceService;

@Component
@Slf4j
@RequiredArgsConstructor
public class HomeServiceListener {

    private final DeviceService deviceService;

    @KafkaListener(topics = "HOME_DELETED", groupId = "test")
    public void deleteDevicesByHomeId(Integer homeId) {
        deviceService.deleteByHomeId(homeId);
        log.info("Delete devices with homeId={}", homeId);
    }

    @KafkaListener(topics = "ROOM_DELETED", groupId = "test")
    public void deleteDevicesByRoomId(Integer roomId) {
        deviceService.resetRoomId(roomId);
        log.info("Reset room for devices with roomId={}", roomId);
    }
}
