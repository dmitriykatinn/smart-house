package ru.handh.deviceservice.entity;

public enum CapabilityCode {
    SWITCH_LED, TEMPERATURE, COLOR, BRIGHTNESS
}
