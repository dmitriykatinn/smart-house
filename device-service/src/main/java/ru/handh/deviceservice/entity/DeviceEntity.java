package ru.handh.deviceservice.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "devices")
@Data
@NoArgsConstructor
public class DeviceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String tuyaDeviceId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer homeId;

    @Column
    private Integer roomId;

    @Column(nullable = false)
    private Integer ownerId;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DeviceCategory category;

    @OneToMany(mappedBy = "device", cascade = CascadeType.REMOVE)
    private List<CapabilityEntity> capabilities = Collections.emptyList();
}
