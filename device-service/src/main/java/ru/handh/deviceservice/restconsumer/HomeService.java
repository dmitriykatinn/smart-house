package ru.handh.deviceservice.restconsumer;

public interface HomeService {
    Boolean hasAccessToHomeAndRoom(Integer homeId, Integer roomId);
}
