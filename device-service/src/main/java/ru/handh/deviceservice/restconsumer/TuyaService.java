package ru.handh.deviceservice.restconsumer;

public interface TuyaService {
    boolean checkIfExists(String tuyaDeviceId);

    String getDeviceName(String tuyaDeviceId);
}
