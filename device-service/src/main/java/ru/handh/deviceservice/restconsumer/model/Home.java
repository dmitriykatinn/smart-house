package ru.handh.deviceservice.restconsumer.model;

import lombok.Data;

import java.util.List;

@Data
public class Home {

    private Integer id;
    private String name;
    private String address;
    private List<Room> rooms;
}
