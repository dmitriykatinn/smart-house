package ru.handh.deviceservice.restconsumer.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import ru.handh.deviceservice.restconsumer.HomeService;
import ru.handh.deviceservice.restconsumer.model.Home;
import ru.handh.deviceservice.util.AuthorizationUtil;

@Service
@RequiredArgsConstructor
public class HomeServiceImpl implements HomeService {

    @Value("${home-service.path}")
    private String servicePath;
    private final AuthorizationUtil authorizationUtil;

    @Override
    public Boolean hasAccessToHomeAndRoom(Integer homeId, Integer roomId) {
        try {
            Home home = getHome(homeId);
            if (roomId != null && home.getRooms().stream().noneMatch(room -> room.getId().equals(roomId))) {
                return false;
            }
        } catch (WebClientResponseException exception) {
            return false;
        }
        return true;
    }

    private Home getHome(Integer homeId) {
        return WebClient.builder()
                .defaultHeader("X-Access-Token", authorizationUtil.getCurrentToken())
                .build()
                .get()
                .uri(servicePath + "/api/homes/{homeId}", homeId)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Home.class)
                .block();
    }
}
