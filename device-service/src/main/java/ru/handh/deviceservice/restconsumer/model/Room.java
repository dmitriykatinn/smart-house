package ru.handh.deviceservice.restconsumer.model;

import lombok.Data;

@Data
public class Room {

    private Integer id;
    private String name;
}
