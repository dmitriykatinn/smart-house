package ru.handh.deviceservice.service;

import ru.handh.deviceservice.dto.response.CapabilitySimple;
import ru.handh.deviceservice.entity.DeviceEntity;

import java.util.List;

public interface CapabilityService {
    void create(List<CapabilitySimple> capabilities, DeviceEntity device);
}
