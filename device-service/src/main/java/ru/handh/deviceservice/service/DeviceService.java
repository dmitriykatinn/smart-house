package ru.handh.deviceservice.service;

import org.hibernate.engine.spi.PersistenceContext;
import ru.handh.deviceservice.dto.request.DeviceCreationRequest;
import ru.handh.deviceservice.dto.request.DeviceEditingRequest;
import ru.handh.deviceservice.entity.DeviceEntity;

import java.util.Arrays;
import java.util.List;

public interface DeviceService {
    DeviceEntity createDevice(DeviceCreationRequest request, int ownerId);

    DeviceEntity findById(int deviceId);

    DeviceEntity edit(int deviceId, DeviceEditingRequest request);

    List<DeviceEntity> findAll();

    void delete(DeviceEntity device);

    void deleteByOwnerId(Integer ownerId);

    void deleteByHomeId(Integer homeId);

    void resetRoomId(Integer roomId);
}
