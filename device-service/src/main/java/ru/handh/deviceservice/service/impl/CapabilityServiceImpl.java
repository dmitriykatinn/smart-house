package ru.handh.deviceservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import ru.handh.deviceservice.dto.response.CapabilitySimple;
import ru.handh.deviceservice.entity.CapabilityEntity;
import ru.handh.deviceservice.entity.DeviceEntity;
import ru.handh.deviceservice.repository.CapabilityRepository;
import ru.handh.deviceservice.service.CapabilityService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CapabilityServiceImpl implements CapabilityService {

    private final CapabilityRepository capabilityRepository;
    private final ConversionService conversionService;

    @Override
    public void create(List<CapabilitySimple> capabilities, DeviceEntity device) {
        capabilityRepository.saveAll(capabilities.stream()
                .map(c -> {
                    CapabilityEntity capability = conversionService.convert(c, CapabilityEntity.class);
                    capability.setDevice(device);
                    return capability;
                })
                .collect(Collectors.toList())
        );
    }
}
