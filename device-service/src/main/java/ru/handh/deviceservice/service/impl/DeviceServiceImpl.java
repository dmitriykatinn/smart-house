package ru.handh.deviceservice.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.handh.deviceservice.dto.request.DeviceCreationRequest;
import ru.handh.deviceservice.dto.request.DeviceEditingRequest;
import ru.handh.deviceservice.entity.DeviceCategory;
import ru.handh.deviceservice.entity.DeviceEntity;
import ru.handh.deviceservice.repository.DeviceRepository;
import ru.handh.deviceservice.service.DeviceService;
import ru.handh.deviceservice.restconsumer.HomeService;
import ru.handh.deviceservice.restconsumer.TuyaService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeviceServiceImpl implements DeviceService {

    private final DeviceRepository deviceRepository;
    private final ConversionService conversionService;
    private final TuyaService tuyaService;
    private final HomeService homeService;

    @Override
    public DeviceEntity createDevice(DeviceCreationRequest request, int ownerId) {
        if (!tuyaService.checkIfExists(request.getTuyaDeviceId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device with this tuyaDeviceId doesn't exist");
        }
        if (!homeService.hasAccessToHomeAndRoom(request.getHomeId(), request.getRoomId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        DeviceEntity device = conversionService.convert(request, DeviceEntity.class);
        if (device.getName() == null) {
            String nameFromTuya = tuyaService.getDeviceName(device.getTuyaDeviceId());
            device.setName(nameFromTuya);
        }
        device.setOwnerId(ownerId);
        device.setCategory(DeviceCategory.LIGHT);
        return deviceRepository.save(device);
    }

    @Override
    public DeviceEntity findById(int deviceId) {
        return deviceRepository.findById(deviceId).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device with this id doesn't exist"));
    }

    @Override
    public DeviceEntity edit(int deviceId, DeviceEditingRequest request) {
        if (!homeService.hasAccessToHomeAndRoom(request.getHomeId(), request.getRoomId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        DeviceEntity device = findById(deviceId);
        device.setName(request.getName());
        device.setHomeId(request.getHomeId());
        device.setRoomId(request.getRoomId());
        return deviceRepository.save(device);
    }

    @Override
    public List<DeviceEntity> findAll() {
        return deviceRepository.findAll();
    }

    @Override
    @Transactional
    public void delete(DeviceEntity device) {
        deviceRepository.delete(device);
    }

    @Override
    @Transactional
    public void deleteByOwnerId(Integer ownerId) {
        deviceRepository.deleteByOwnerId(ownerId);
    }

    @Override
    @Transactional
    public void deleteByHomeId(Integer homeId) {
        deviceRepository.deleteByHomeId(homeId);
    }

    @Override
    public void resetRoomId(Integer roomId) {
        deviceRepository.findByRoomId(roomId).forEach(d -> {
            d.setRoomId(null);
            deviceRepository.save(d);
        });
    }

}
